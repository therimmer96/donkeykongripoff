﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))] //Auto adds rigidybody to an object if one is not present when this is added.
public class CharMove : MonoBehaviour
{
	public float moveSpeed = 5;
	public float jumpSpeed = 5;
	public float ladderSpeed = 5;

	[SerializeField]
	private GameObject model;

	[SerializeField]
	private Animator animator;

	private bool inLadderArea = false;
	private bool onLadder = false;
	private bool inLadderTop = false;
	private bool onGround = false;

	private GameObject currentLadderTop;

	private Rigidbody rb;

	// Use this for initialization
	void Start()
	{
		rb = GetComponent<Rigidbody>(); //Get a reference to the rigidybody component
		rb.constraints = RigidbodyConstraints.FreezeRotation; //Locks the rotation so that the character doesn't fall over and break his face
	}

	// Update is called once per frame
	void Update()
	{
		#region Basic Character Movement

		Vector3 newVelocity = Vector3.zero;

		newVelocity.y = rb.velocity.y; 

		if (onGround)
		{
			rb.useGravity = true;
			////Move along X axis at moveSpeed, set Y velocity to existing velocity to not cancel it. Input is rounded to make the input behave as buttons.
			//newVelocity.x = moveSpeed * Mathf.Round(Input.GetAxisRaw("Horizontal"));

			if (Input.GetAxisRaw("Horizontal") != 0)
			{
				animator.Play("Mario Walk");

				if (Input.GetAxisRaw("Horizontal") > 0)
				{
					newVelocity.x = moveSpeed;
				}

				if (Input.GetAxisRaw("Horizontal") < 0)
				{
					newVelocity.x = -moveSpeed;
				} 
			}
			else
			{
				animator.Play("MarioIdle");
			}
		}
		else
		{
			newVelocity = rb.velocity;
			animator.Play("MarioIdle");
		}

		if (Input.GetAxisRaw("Horizontal") != 0)
			model.transform.rotation = Quaternion.LookRotation(Vector3.right * -Mathf.Round(Input.GetAxisRaw("Horizontal"))); //set the rotation to match movement

		if (!onLadder && onGround) //if not on ladder
		{
			if (Input.GetAxis("Jump") != 0)
				newVelocity.y = jumpSpeed; //Set jump velocity
		}

		rb.velocity = newVelocity; //apply the new velocity 
		#endregion

		#region Ladders

		if (onLadder)
		{
			newVelocity.y = 0; //reset updown velocity because we're on a ladder
		}

		if (Input.GetAxisRaw("Vertical") > 0)
		{
			if (inLadderArea)
			{
				newVelocity = Vector3.zero;
				rb.useGravity = false;
				onLadder = true;
				newVelocity.y = ladderSpeed; //Set climb velocity
			}

			if (inLadderTop && onLadder)
			{
				transform.position = new Vector3(transform.position.x, transform.position.y, currentLadderTop.transform.position.z);
				rb.useGravity = true;
				onLadder = false;
			}
		}
		if (Input.GetAxisRaw("Vertical") < 0)
		{
			if (inLadderTop && onGround)
			{
				newVelocity = Vector3.zero;
				transform.position = new Vector3(transform.position.x, transform.position.y, currentLadderTop.transform.position.z -1);
				rb.useGravity = false;
				onLadder = true;
				newVelocity.y = -ladderSpeed; //Set climb velocity
			}
			if (inLadderArea)
			{
				newVelocity.y = -ladderSpeed; //Set climb velocity
			}
		}
		#endregion

		rb.velocity = newVelocity; //apply the new velocity 

	}

	#region Triggers
	private void OnTriggerEnter(Collider other)
	{

		if (other.tag == "Ladder")
		{
			inLadderArea = true;
		}
		else if (other.tag == "LadderTop")
		{
			inLadderTop = true;
			currentLadderTop = other.gameObject;
		}
		else if(other.tag != "player")
		{
			onGround = true;
		}
	}

	private void OnTriggerExit(Collider other)
	{


		if (other.tag == "Ladder")
		{
			inLadderArea = false;
			onLadder = false;
			rb.useGravity = true;
		}
		else if (other.tag == "LadderTop")
		{
			inLadderTop = false;
		}
		else if (other.tag != "Player")
		{
			onGround = false;
		}
		#endregion
	}
}
